//console.log("Activity S33");

// fetch("https://jsonplaceholder.typicode.com/todos")
// .then(response => response.json())
// .then(json => console.log(json))


// GET HTTP method
async function fetchTitle() {

	let res = await fetch("https://jsonplaceholder.typicode.com/todos") 

	let  json2 = await res.json()
	// console.log(json2)
	
	let titlesOnly = await json2.map(function(titleArr) {
    return titleArr.title;
}); console.log(titlesOnly);
}

fetchTitle();



// // GET HTTP method

async function fetchData() {

	let response = await fetch("https://jsonplaceholder.typicode.com/todos/1") 

	let  json = await response.json()
	console.log(`The ${json.title} has a status of ${json.completed}`);
}

fetchData();


// POST HTTP method

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
	    id: 201,
	    title: "Created To Do List Item",
	    completed: false
	})
})
.then(response => response.json())
.then(json => console.log(json))

// PUT HTTP method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1,
		id: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// PATCH HTTP method

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then(response => response.json())
.then(json => console.log(json))

// DELETE HTTP method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})